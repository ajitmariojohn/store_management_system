module itemMgmt.presentation {
    requires com.sms.framework.commons;
    requires spring.web;
    requires spring.beans;
    requires itemMgmt.domain;
}